﻿#undef DEBUG

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UI_wifikick;

namespace UI_wifikick
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        private string txt;

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
            IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);

        private PrivateFontCollection fonts = new PrivateFontCollection();

        Font myFont;

        public Form1()
        {
            InitializeComponent();
            //this.FormBorderStyle = FormBorderStyle.None;
            FormClosing += new FormClosingEventHandler(closing);


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle; // 크기 고정   
            this.MaximizeBox = false; // 최대화 버튼 무효
            this.FormBorderStyle = FormBorderStyle.None;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width / 2 - this.Size.Width / 2, Screen.PrimaryScreen.Bounds.Height / 2 - this.Size.Height / 2);

            string[] data = { "역삼본사(MOBIS WiFi)", "마북연구소 제동시험동(MOBIS WiFi BT)", "마북연구소 의장연구동(MOBIS WiFi TP)", "마북연구소 샤시연구동(MOBIS WiFi CH)", "마북연구소 EE연구동/시험동(MOBIS WiFi EE)", "마북연구소 제어동(MOBIS WiFi SW)", "의왕연구소(MOBIS WiFi EW)", "서산연구소(MOBIS WiFi SS)" };

            comboBox1.Items.AddRange(data);
            comboBox1.SelectedIndex = 0;

            byte[] fontData = Properties.Resources.BinggraeMelona;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.BinggraeMelona.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.BinggraeMelona.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);

            myFont = new Font(fonts.Families[0], 9f);

            label1.Font = myFont;

            comboBox1.Font = myFont;

            button2.Font = myFont;
            button3.Font = myFont;



            Font font1 = new Font(fonts.Families[0], 15f);
            label7.Font = font1;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (comboBox1.SelectedIndex >= 0)
            {
                txt = comboBox1.SelectedItem as string;
                //MessageBox.Show(txt);
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(txt);
            if (txt.Equals("역삼본사(MOBIS WiFi)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }

            }
            else if (txt.Equals("마북연구소 제동시험동(MOBIS WiFi BT)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("마북연구소 의장연구동(MOBIS WiFi TP)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("마북연구소 샤시연구동(MOBIS WiFi CH)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("마북연구소 EE연구동/시험동(MOBIS WiFi EE)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("마북연구소 제어동(MOBIS WiFi SW)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("의왕연구소(MOBIS WiFi EW)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("서산연구소(MOBIS WiFi SS)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else
            {
                MessageBox.Show("값을 잘못 선택했습니다.", "안내"
                                    , MessageBoxButtons.OK
                                    , MessageBoxIcon.Information);
                return;
            }



            this.Visible = false;             // 추가
            Form3 showForm3 = new Form3();
            showForm3.ShowDialog();
            this.Close();
        }



        private void closing(object sender, FormClosingEventArgs e)
        {
            /*
            // 폼을 닫을건지 취소할건지 결정
            DialogResult dr = MessageBox.Show("종료하시겠습니까?", "종료확인"
                                    , MessageBoxButtons.YesNo
                                    , MessageBoxIcon.Information);
            if (dr == DialogResult.No)
            {
                e.Cancel = true; // 취소
            }
            */
        }

        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }
        private void comboBox1_KeyDown(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }
        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }



        private void button3_Click(object sender, EventArgs e)
        {
            this.button3.Text = "실행 중...";

            if (txt.Equals("역삼본사(MOBIS WiFi)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }

            }
            else if (txt.Equals("마북연구소 제동시험동(MOBIS WiFi BT)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }

            else if (txt.Equals("마북연구소 의장연구동(MOBIS WiFi TP)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("마북연구소 샤시연구동(MOBIS WiFi CH)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("마북연구소 EE연구동/시험동(MOBIS WiFi EE)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("마북연구소 제어동(MOBIS WiFi SW)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("의왕연구소(MOBIS WiFi EW)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else if (txt.Equals("서산연구소(MOBIS WiFi SS)"))
            {
                if (Program.Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);
                    Program.Profile = spstring[1].Replace(")", "");
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
                else
                {
                    string profile = txt.ToString();
                    char sp = '(';
                    string[] spstring = profile.Split(sp);

                    string tmp = spstring[1].Replace(")", "");
                    tmp = tmp + "_AD";
                    Program.Profile = tmp;
#if DEBUG
                    Program.TXTLog("선택한 정보: " + Program.Profile);
#endif
                }
            }
            else
            {
                MessageBox.Show("값을 잘못 선택했습니다.", "안내"
                                    , MessageBoxButtons.OK
                                    , MessageBoxIcon.Information);
                return;
            }

            Form0.changeDHCP();
            this.button3.Text = "실행";
        }
    }
}