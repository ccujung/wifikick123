﻿#undef DEBUG

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using UI_wifikick;
using NativeWifi;


namespace UI_wifikick
{  
    public partial class Form0 : MetroFramework.Forms.MetroForm
    {
        static string serviceID = string.Empty;
        static string index = string.Empty;
        private string txt;

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
            IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);

        private PrivateFontCollection fonts = new PrivateFontCollection();

        Font myFont;

        public Form0()
        {
            InitializeComponent();
            //this.FormBorderStyle = FormBorderStyle.None;
            //FormClosing += new FormClosingEventHandler(closing);
        }

        private void Form0_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle; // 크기 고정   
            this.MaximizeBox = false; // 최대화 버튼 무효
            this.FormBorderStyle = FormBorderStyle.None;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width / 2 - this.Size.Width / 2, Screen.PrimaryScreen.Bounds.Height / 2 - this.Size.Height / 2);
        }

        private void button1_Click(object sender, EventArgs e) // IP 설정
        {
            this.Visible = false;             // 추가
            Form1 showForm1 = new Form1();
            showForm1.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) // 외부 DHCP
        {
            
        }

        public static void changeDHCP()
        {

            string setXml = string.Empty;
            Form3.FindReg();
            Form3.SetWifiID();
            //Form3.SetWifiActive();

            switch (Program.Profile.ToString())
            {
                case "MOBIS WiFi":
                    setXml = Properties.Resources.mobiswifi;
                    break;

                case "MOBIS WiFi_AD":
                    setXml = Properties.Resources.mobiswifi_AD;
                    break;

                case "MOBIS WiFi BT":
                    setXml = Properties.Resources.mobiswifi_BT;
                    break;

                case "MOBIS WiFi BT_AD":
                    setXml = Properties.Resources.mobiswifi_BT_AD;
                    break;

                case "MOBIS WiFi TP":
                    setXml = Properties.Resources.mobiswifi_TP;
                    break;

                case "MOBIS WiFi TP_AD":
                    setXml = Properties.Resources.mobiswifi_TP_AD;
                    break;

                case "MOBIS WiFi CH":
                    setXml = Properties.Resources.mobiswifi_CH;
                    break;

                case "MOBIS WiFi CH_AD":
                    setXml = Properties.Resources.mobiswifi_CH_AD;
                    break;

                case "MOBIS WiFi EE":
                    setXml = Properties.Resources.mobiswifi_EE;
                    break;

                case "MOBIS WiFi EE_AD":
                    setXml = Properties.Resources.mobiswifi_EE_AD;
                    break;

                case "MOBIS WiFi SW":
                    setXml = Properties.Resources.mobiswifi_SW;
                    break;

                case "MOBIS WiFi SW_AD":
                    setXml = Properties.Resources.mobiswifi_SW_AD;
                    break;

                case "MOBIS WiFi EW":
                    setXml = Properties.Resources.mobiswifi_EW;
                    break;

                case "MOBIS WiFi EW_AD":
                    setXml = Properties.Resources.mobiswifi_EW_AD;
                    break;

                case "MOBIS WiFi SS":
                    setXml = Properties.Resources.mobiswifi_SS;
                    break;

                case "MOBIS WiFi SS_AD":
                    setXml = Properties.Resources.mobiswifi_SS_AD;
                    break;

                default:
#if DEBUG
                    Program.TXTLog(Program.Profile);
#endif
                    break;
            }

            WlanClient client = new WlanClient();

            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {
                if (wlanIface.InterfaceDescription.Equals(Program.Desc))
                {
                    try
                    {
                        /* 
                         * 1. 기존 profile 삭제하는 메소드인데 삭제하는걸 넣어야하는지??? 말아야하는지 사용자 측면 고려해봐야함
                         * 2020-06-24 주석처리
                         * 
                        foreach (Wlan.WlanProfileInfo info in wlanIface.GetProfiles())
                        {
                            if (info.profileName != null)
                            {
                                wlanIface.DeleteProfile(info.profileName);
                                Program.TXTLog("삭제한 profile: " + info.profileName);
                                Form3.Delay(250);
                            }
                        }
                        
                        wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, setXml, true);
                        Program.TXTLog("프로파일 적용 완료");
                        */
                        if (Program.Profile.IndexOf("_AD", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            try
                            {
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_AD, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_AD 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_BT, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_AD 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_CH_AD, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_CH_AD 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_EE, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_EE1_AD 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_EW_AD, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_EW_AD 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_SS_AD, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_SS_AD 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_SW_AD, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_SW_AD 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_TP_AD, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_TP_AD 적용 완료");
#endif
                            }
                            catch (System.ArgumentException ex)
                            {
#if DEBUG
                                Program.TXTLog(ex.Message);
#endif
                            }
                            catch (Exception ex)
                            {
#if DEBUG
                                Program.TXTLog(ex.Message);
#endif
                                //Console.WriteLine("Error Occured!");
                                //throw;
                            }
                        }
                        else
                        {
                            try
                            {
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi, true);
#if DEBUG
                                Program.TXTLog("mobiswifi 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_BT, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_dhcp 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_CH, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_CH 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_EE, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_EE1 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_EW, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_EW 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_SS, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_SS 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_SW, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_SW 적용 완료");
#endif
                                wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, Properties.Resources.mobiswifi_TP, true);
#if DEBUG
                                Program.TXTLog("mobiswifi_TP 적용 완료");
#endif
                            }

                            catch (System.ArgumentException ex)
                            {
#if DEBUG
                                Program.TXTLog(ex.Message);
#endif
                            }
                            catch (Exception ex)
                            {
#if DEBUG
                                Program.TXTLog(ex.Message);
#endif
                                //Console.WriteLine("Error Occured!");
                                //throw;
                            }
                        }
                    }
                    catch (System.ArgumentException ex)
                    {
#if DEBUG
                        Program.TXTLog(ex.Message);
#endif
                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        Program.TXTLog(ex.Message);
#endif
                        //Console.WriteLine("Error Occured!");
                        //throw;
                    }
                    try
                    {
                        if (Program.Profile.IndexOf("_AD", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            string tmp = Program.Profile.Replace("_AD", "");
#if DEBUG
                            Program.TXTLog(tmp);
#endif
                            wlanIface.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, tmp.ToString());
                        }
                        else
                        {
                            wlanIface.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, Program.Profile.ToString());
                        }
#if DEBUG
                        Program.TXTLog("무선랜에 XML설정 " + wlanIface.ToString());
#endif
                    }
                    catch (System.ArgumentException ex)
                    {
#if DEBUG
                        Program.TXTLog(ex.Message);
#endif
                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        Program.TXTLog(ex.Message);
#endif
                        //Console.WriteLine("Error Occured!");
                        //throw;
                    }
                    
                    int timeout = 3000;

                    try
                    {

                        while ((wlanIface.InterfaceState.ToString() != "Connected") && (timeout >= 0))
                        {
                            try
                            {
#if DEBUG
                                Program.TXTLog("중간 인터페이스 상태: " + wlanIface.InterfaceState.ToString());
#endif
                            }
                            catch (System.ArgumentException ex)
                            {
#if DEBUG
                                Program.TXTLog(ex.Message);
#endif
                            }
                            catch (Exception ex)
                            {
#if DEBUG
                                Program.TXTLog("중간 인터페이스 상태찍는거에서 에러, wlanIface.InterfaceState.ToString()");
                                Program.TXTLog(ex.Message);
#endif
                            }
                            try
                            {
                                //System.Threading.Thread.Sleep(500);
#if DEBUG
                                Program.TXTLog("Delay 전");
#endif
                                Form3.Delay(250);
#if DEBUG
                                Program.TXTLog("Delay 후");
#endif
                            }
                            catch (System.ArgumentException ex)
                            {
#if DEBUG
                                Program.TXTLog(ex.Message);
#endif
                            }
                            catch (Exception ex)
                            {
#if DEBUG
                                Program.TXTLog("중간 인터페이스 while문에서 exception");
                                Program.TXTLog(ex.Message);
#endif
                            }
                            timeout = timeout - 500;
                        }
                    }
                    catch (System.ArgumentException ex)
                    {
#if DEBUG
                        Program.TXTLog(ex.Message);
#endif
                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        Program.TXTLog(ex.Message);
#endif
                    }
                    

                    //Program.TXTLog("최종 " + wlanIface.InterfaceState.ToString() + wlanIface);

                    if (wlanIface.InterfaceState.ToString() == "Connected")
                    {
#if DEBUG
                        Program.TXTLog("무선랜 인터페이스: Connected");
#endif
                    }
                    else
                    {
                       // richTextBox1.AppendText("무선랜장치 활성화");
                    }
                }
            }


            NetworkInterface[] networks = null;
            string service_name = string.Empty;
            try
            {
                networks = NetworkInterface.GetAllNetworkInterfaces();
            }
            catch (Exception ex)
            {
#if DEBUG
                Program.TXTLog(ex.Message);
#endif
            }
            try
            {
                serviceID = Form3.getServiceId();
                foreach (NetworkInterface net in networks)
                {
                    if (serviceID.Equals(net.Id))
                    {
                        service_name = net.Name;
#if DEBUG
                        Program.TXTLog("설정할 네트워크 이름: " + service_name);
#endif
                        break;
                    }
                    else
                    {
#if DEBUG
                        Program.TXTLog("비대상 네트워크 이름: " + net.Name);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Program.TXTLog(ex.Message);
#endif
            }


            if (Program.OSName.Equals("Windows 7"))
            {
                string argumentsIP = "interface ip set address \"" + service_name + "\" dhcp";
                try
                {
                    ProcessStartInfo procStartInfoIP = new ProcessStartInfo("netsh", argumentsIP);
                    procStartInfoIP.RedirectStandardOutput = true;
                    procStartInfoIP.UseShellExecute = false;
                    procStartInfoIP.CreateNoWindow = true;

                    Process proc = Process.Start(procStartInfoIP);
                    proc.WaitForExit();
#if DEBUG
                    Program.TXTLog(proc.ExitCode.ToString());
#endif
                }
                catch (Exception ex)
                {
#if DEBUG
                    Program.TXTLog(ex.Message);
#endif
                }

                string argumentsDNS = "interface ip set dnsservers \"" + service_name + "\" dhcp";
                //Program.TXTLog("윈7 DNS설정 명령어 : " + argumentsDNS);

                try
                {
                    ProcessStartInfo procStartInfoDNS = new ProcessStartInfo("netsh", argumentsDNS);
                    procStartInfoDNS.RedirectStandardOutput = true;
                    procStartInfoDNS.UseShellExecute = false;
                    procStartInfoDNS.CreateNoWindow = true;

                    Process proc = Process.Start(procStartInfoDNS);
                    proc.WaitForExit();
#if DEBUG
                    Program.TXTLog(proc.ExitCode.ToString());
#endif
                }
                catch (Exception ex)
                {
#if DEBUG
                    Program.TXTLog(ex.Message);
#endif
                }
            }
            else
            {
                string argumentsIP = "-c int ip set address name=\"" + service_name + "\" dhcp";
                try
                {
                    ProcessStartInfo procStartInfoIP = new ProcessStartInfo("netsh", argumentsIP);
                    procStartInfoIP.RedirectStandardOutput = true;
                    procStartInfoIP.UseShellExecute = false;
                    procStartInfoIP.CreateNoWindow = true;  

                    Process proc = Process.Start(procStartInfoIP);
                    proc.WaitForExit();
#if DEBUG
                    Program.TXTLog(proc.ExitCode.ToString());
#endif
                }
                catch (Exception ex)
                {
#if DEBUG
                    Program.TXTLog(ex.Message);
#endif
                }

                string argumentsDNS = "-c int ip set dns name=\"" + service_name + "\" dhcp";
                //Program.TXTLog("윈7 DNS설정 명령어 : " + argumentsDNS);

                try
                {
                    ProcessStartInfo procStartInfoDNS = new ProcessStartInfo("netsh", argumentsDNS);
                    procStartInfoDNS.RedirectStandardOutput = true;
                    procStartInfoDNS.UseShellExecute = false;
                    procStartInfoDNS.CreateNoWindow = true;

                    Process proc = Process.Start(procStartInfoDNS);
                    proc.WaitForExit();
#if DEBUG
                    Program.TXTLog(proc.ExitCode.ToString());
#endif
                }
                catch (Exception ex)
                {
#if DEBUG
                    Program.TXTLog(ex.Message);
#endif
                }
            }
            MessageBox.Show(new Form { TopMost = true }, "무선WIFI 설정 완료", "안내", MessageBoxButtons.OK);
        }
    }
}
