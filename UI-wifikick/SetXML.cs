﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace WAPI
{
    /// <summary>
    /// WifiAPI에 대한 요약 설명입니다.
    /// </summary>
    class WifiAPI
    {




        /*
         typedef struct _WLAN_PROFILE_INFO { 
          WCHAR strProfileName[256]; 
          DWORD dwFlags;
         } WLAN_PROFILE_INFO, 
         *PWLAN_PROFILE_INFO;
        */
        [StructLayout(LayoutKind.Sequential)]
        public struct WLAN_PROFILE_INFO
        {
            [MarshalAs(UnmanagedType.LPWStr, SizeConst = 256)]
            public String strProfileName; // string ma mat dlzku 256
            [MarshalAs(UnmanagedType.I4)]
            public Int32 dwFlags;
        }



        [Flags]
        public enum WlanProfileFlags
        {
            /// <remarks>
            /// The only option available on Windows XP SP2.
            /// </remarks>
            AllUser = 0,
            GroupPolicy = 1,
            User = 2
        }


        [Flags]
        public enum WlanAccess
        {
            /// <summary>
            /// The user can view profile permissions.
            /// </summary>
            ReadAccess = 0x00020000 | 0x0001,
            /// <summary>
            /// The user has read access, and the user can also connect to and disconnect from a network using the profile.
            /// </summary>
            ExecuteAccess = ReadAccess | 0x0020,
            /// <summary>
            /// The user has execute access and the user can also modify and delete permissions associated with a profile.
            /// </summary>
            WriteAccess = ReadAccess | ExecuteAccess | 0x0002 | 0x00010000 | 0x00040000
        }


        public enum WlanReasonCode
        {
            Success = 0,
            // general codes
            UNKNOWN = 0x10000 + 1,

            RANGE_SIZE = 0x10000,
            BASE = 0x10000 + RANGE_SIZE,

            // range for Auto Config
            //
            AC_BASE = 0x10000 + RANGE_SIZE,
            AC_CONNECT_BASE = (AC_BASE + RANGE_SIZE / 2),
            AC_END = (AC_BASE + RANGE_SIZE - 1),

            // range for profile manager
            // it has profile adding failure reason codes, but may not have
            // connection reason codes
            //
            PROFILE_BASE = 0x10000 + (7 * RANGE_SIZE),
            PROFILE_CONNECT_BASE = (PROFILE_BASE + RANGE_SIZE / 2),
            PROFILE_END = (PROFILE_BASE + RANGE_SIZE - 1),

            // range for MSM
            //
            MSM_BASE = 0x10000 + (2 * RANGE_SIZE),
            MSM_CONNECT_BASE = (MSM_BASE + RANGE_SIZE / 2),
            MSM_END = (MSM_BASE + RANGE_SIZE - 1),

            // range for MSMSEC
            //
            MSMSEC_BASE = 0x10000 + (3 * RANGE_SIZE),
            MSMSEC_CONNECT_BASE = (MSMSEC_BASE + RANGE_SIZE / 2),
            MSMSEC_END = (MSMSEC_BASE + RANGE_SIZE - 1),

            // AC network incompatible reason codes
            //
            NETWORK_NOT_COMPATIBLE = (AC_BASE + 1),
            PROFILE_NOT_COMPATIBLE = (AC_BASE + 2),

            // AC connect reason code
            //
            NO_AUTO_CONNECTION = (AC_CONNECT_BASE + 1),
            NOT_VISIBLE = (AC_CONNECT_BASE + 2),
            GP_DENIED = (AC_CONNECT_BASE + 3),
            USER_DENIED = (AC_CONNECT_BASE + 4),
            BSS_TYPE_NOT_ALLOWED = (AC_CONNECT_BASE + 5),
            IN_FAILED_LIST = (AC_CONNECT_BASE + 6),
            IN_BLOCKED_LIST = (AC_CONNECT_BASE + 7),
            SSID_LIST_TOO_LONG = (AC_CONNECT_BASE + 8),
            CONNECT_CALL_FAIL = (AC_CONNECT_BASE + 9),
            SCAN_CALL_FAIL = (AC_CONNECT_BASE + 10),
            NETWORK_NOT_AVAILABLE = (AC_CONNECT_BASE + 11),
            PROFILE_CHANGED_OR_DELETED = (AC_CONNECT_BASE + 12),
            KEY_MISMATCH = (AC_CONNECT_BASE + 13),
            USER_NOT_RESPOND = (AC_CONNECT_BASE + 14),

            // Profile validation errors
            //
            INVALID_PROFILE_SCHEMA = (PROFILE_BASE + 1),
            PROFILE_MISSING = (PROFILE_BASE + 2),
            INVALID_PROFILE_NAME = (PROFILE_BASE + 3),
            INVALID_PROFILE_TYPE = (PROFILE_BASE + 4),
            INVALID_PHY_TYPE = (PROFILE_BASE + 5),
            MSM_SECURITY_MISSING = (PROFILE_BASE + 6),
            IHV_SECURITY_NOT_SUPPORTED = (PROFILE_BASE + 7),
            IHV_OUI_MISMATCH = (PROFILE_BASE + 8),
            // IHV OUI not present but there is IHV settings in profile
            IHV_OUI_MISSING = (PROFILE_BASE + 9),
            // IHV OUI is present but there is no IHV settings in profile
            IHV_SETTINGS_MISSING = (PROFILE_BASE + 10),
            // both/conflict MSMSec and IHV security settings exist in profile
            CONFLICT_SECURITY = (PROFILE_BASE + 11),
            // no IHV or MSMSec security settings in profile
            SECURITY_MISSING = (PROFILE_BASE + 12),
            INVALID_BSS_TYPE = (PROFILE_BASE + 13),
            INVALID_ADHOC_CONNECTION_MODE = (PROFILE_BASE + 14),
            NON_BROADCAST_SET_FOR_ADHOC = (PROFILE_BASE + 15),
            AUTO_SWITCH_SET_FOR_ADHOC = (PROFILE_BASE + 16),
            AUTO_SWITCH_SET_FOR_MANUAL_CONNECTION = (PROFILE_BASE + 17),
            IHV_SECURITY_ONEX_MISSING = (PROFILE_BASE + 18),
            PROFILE_SSID_INVALID = (PROFILE_BASE + 19),
            TOO_MANY_SSID = (PROFILE_BASE + 20),

            // MSM network incompatible reasons
            //
            UNSUPPORTED_SECURITY_SET_BY_OS = (MSM_BASE + 1),
            UNSUPPORTED_SECURITY_SET = (MSM_BASE + 2),
            BSS_TYPE_UNMATCH = (MSM_BASE + 3),
            PHY_TYPE_UNMATCH = (MSM_BASE + 4),
            DATARATE_UNMATCH = (MSM_BASE + 5),

            // MSM connection failure reasons, to be defined
            // failure reason codes
            //
            // user called to disconnect
            USER_CANCELLED = (MSM_CONNECT_BASE + 1),
            // got disconnect while associating
            ASSOCIATION_FAILURE = (MSM_CONNECT_BASE + 2),
            // timeout for association
            ASSOCIATION_TIMEOUT = (MSM_CONNECT_BASE + 3),
            // pre-association security completed with failure
            PRE_SECURITY_FAILURE = (MSM_CONNECT_BASE + 4),
            // fail to start post-association security
            START_SECURITY_FAILURE = (MSM_CONNECT_BASE + 5),
            // post-association security completed with failure
            SECURITY_FAILURE = (MSM_CONNECT_BASE + 6),
            // security watchdog timeout
            SECURITY_TIMEOUT = (MSM_CONNECT_BASE + 7),
            // got disconnect from driver when roaming
            ROAMING_FAILURE = (MSM_CONNECT_BASE + 8),
            // failed to start security for roaming
            ROAMING_SECURITY_FAILURE = (MSM_CONNECT_BASE + 9),
            // failed to start security for adhoc-join
            ADHOC_SECURITY_FAILURE = (MSM_CONNECT_BASE + 10),
            // got disconnection from driver
            DRIVER_DISCONNECTED = (MSM_CONNECT_BASE + 11),
            // driver operation failed
            DRIVER_OPERATION_FAILURE = (MSM_CONNECT_BASE + 12),
            // Ihv service is not available
            IHV_NOT_AVAILABLE = (MSM_CONNECT_BASE + 13),
            // Response from ihv timed out
            IHV_NOT_RESPONDING = (MSM_CONNECT_BASE + 14),
            // Timed out waiting for driver to disconnect
            DISCONNECT_TIMEOUT = (MSM_CONNECT_BASE + 15),
            // An internal error prevented the operation from being completed.
            INTERNAL_FAILURE = (MSM_CONNECT_BASE + 16),
            // UI Request timed out.
            UI_REQUEST_TIMEOUT = (MSM_CONNECT_BASE + 17),
            // Roaming too often, post security is not completed after 5 times.
            TOO_MANY_SECURITY_ATTEMPTS = (MSM_CONNECT_BASE + 18),

            // MSMSEC reason codes
            //

            MSMSEC_MIN = MSMSEC_BASE,

            // Key index specified is not valid
            MSMSEC_PROFILE_INVALID_KEY_INDEX = (MSMSEC_BASE + 1),
            // Key required, PSK present
            MSMSEC_PROFILE_PSK_PRESENT = (MSMSEC_BASE + 2),
            // Invalid key length
            MSMSEC_PROFILE_KEY_LENGTH = (MSMSEC_BASE + 3),
            // Invalid PSK length
            MSMSEC_PROFILE_PSK_LENGTH = (MSMSEC_BASE + 4),
            // No auth/cipher specified
            MSMSEC_PROFILE_NO_AUTH_CIPHER_SPECIFIED = (MSMSEC_BASE + 5),
            // Too many auth/cipher specified
            MSMSEC_PROFILE_TOO_MANY_AUTH_CIPHER_SPECIFIED = (MSMSEC_BASE + 6),
            // Profile contains duplicate auth/cipher
            MSMSEC_PROFILE_DUPLICATE_AUTH_CIPHER = (MSMSEC_BASE + 7),
            // Profile raw data is invalid (1x or key data)
            MSMSEC_PROFILE_RAWDATA_INVALID = (MSMSEC_BASE + 8),
            // Invalid auth/cipher combination
            MSMSEC_PROFILE_INVALID_AUTH_CIPHER = (MSMSEC_BASE + 9),
            // 802.1x disabled when it's required to be enabled
            MSMSEC_PROFILE_ONEX_DISABLED = (MSMSEC_BASE + 10),
            // 802.1x enabled when it's required to be disabled
            MSMSEC_PROFILE_ONEX_ENABLED = (MSMSEC_BASE + 11),
            MSMSEC_PROFILE_INVALID_PMKCACHE_MODE = (MSMSEC_BASE + 12),
            MSMSEC_PROFILE_INVALID_PMKCACHE_SIZE = (MSMSEC_BASE + 13),
            MSMSEC_PROFILE_INVALID_PMKCACHE_TTL = (MSMSEC_BASE + 14),
            MSMSEC_PROFILE_INVALID_PREAUTH_MODE = (MSMSEC_BASE + 15),
            MSMSEC_PROFILE_INVALID_PREAUTH_THROTTLE = (MSMSEC_BASE + 16),
            // PreAuth enabled when PMK cache is disabled
            MSMSEC_PROFILE_PREAUTH_ONLY_ENABLED = (MSMSEC_BASE + 17),
            // Capability matching failed at network
            MSMSEC_CAPABILITY_NETWORK = (MSMSEC_BASE + 18),
            // Capability matching failed at NIC
            MSMSEC_CAPABILITY_NIC = (MSMSEC_BASE + 19),
            // Capability matching failed at profile
            MSMSEC_CAPABILITY_PROFILE = (MSMSEC_BASE + 20),
            // Network does not support specified discovery type
            MSMSEC_CAPABILITY_DISCOVERY = (MSMSEC_BASE + 21),
            // Passphrase contains invalid character
            MSMSEC_PROFILE_PASSPHRASE_CHAR = (MSMSEC_BASE + 22),
            // Key material contains invalid character
            MSMSEC_PROFILE_KEYMATERIAL_CHAR = (MSMSEC_BASE + 23),
            // Wrong key type specified for the auth/cipher pair
            MSMSEC_PROFILE_WRONG_KEYTYPE = (MSMSEC_BASE + 24),
            // "Mixed cell" suspected (AP not beaconing privacy, we have privacy enabled profile)
            MSMSEC_MIXED_CELL = (MSMSEC_BASE + 25),
            // Auth timers or number of timeouts in profile is incorrect
            MSMSEC_PROFILE_AUTH_TIMERS_INVALID = (MSMSEC_BASE + 26),
            // Group key update interval in profile is incorrect
            MSMSEC_PROFILE_INVALID_GKEY_INTV = (MSMSEC_BASE + 27),
            // "Transition network" suspected, trying legacy 802.11 security
            MSMSEC_TRANSITION_NETWORK = (MSMSEC_BASE + 28),
            // Key contains characters which do not map to ASCII
            MSMSEC_PROFILE_KEY_UNMAPPED_CHAR = (MSMSEC_BASE + 29),
            // Capability matching failed at profile (auth not found)
            MSMSEC_CAPABILITY_PROFILE_AUTH = (MSMSEC_BASE + 30),
            // Capability matching failed at profile (cipher not found)
            MSMSEC_CAPABILITY_PROFILE_CIPHER = (MSMSEC_BASE + 31),

            // Failed to queue UI request
            MSMSEC_UI_REQUEST_FAILURE = (MSMSEC_CONNECT_BASE + 1),
            // 802.1x authentication did not start within configured time
            MSMSEC_AUTH_START_TIMEOUT = (MSMSEC_CONNECT_BASE + 2),
            // 802.1x authentication did not complete within configured time
            MSMSEC_AUTH_SUCCESS_TIMEOUT = (MSMSEC_CONNECT_BASE + 3),
            // Dynamic key exchange did not start within configured time
            MSMSEC_KEY_START_TIMEOUT = (MSMSEC_CONNECT_BASE + 4),
            // Dynamic key exchange did not succeed within configured time
            MSMSEC_KEY_SUCCESS_TIMEOUT = (MSMSEC_CONNECT_BASE + 5),
            // Message 3 of 4 way handshake has no key data (RSN/WPA)
            MSMSEC_M3_MISSING_KEY_DATA = (MSMSEC_CONNECT_BASE + 6),
            // Message 3 of 4 way handshake has no IE (RSN/WPA)
            MSMSEC_M3_MISSING_IE = (MSMSEC_CONNECT_BASE + 7),
            // Message 3 of 4 way handshake has no Group Key (RSN)
            MSMSEC_M3_MISSING_GRP_KEY = (MSMSEC_CONNECT_BASE + 8),
            // Matching security capabilities of IE in M3 failed (RSN/WPA)
            MSMSEC_PR_IE_MATCHING = (MSMSEC_CONNECT_BASE + 9),
            // Matching security capabilities of Secondary IE in M3 failed (RSN)
            MSMSEC_SEC_IE_MATCHING = (MSMSEC_CONNECT_BASE + 10),
            // Required a pairwise key but AP configured only group keys
            MSMSEC_NO_PAIRWISE_KEY = (MSMSEC_CONNECT_BASE + 11),
            // Message 1 of group key handshake has no key data (RSN/WPA)
            MSMSEC_G1_MISSING_KEY_DATA = (MSMSEC_CONNECT_BASE + 12),
            // Message 1 of group key handshake has no group key
            MSMSEC_G1_MISSING_GRP_KEY = (MSMSEC_CONNECT_BASE + 13),
            // AP reset secure bit after connection was secured
            MSMSEC_PEER_INDICATED_INSECURE = (MSMSEC_CONNECT_BASE + 14),
            // 802.1x indicated there is no authenticator but profile requires 802.1x
            MSMSEC_NO_AUTHENTICATOR = (MSMSEC_CONNECT_BASE + 15),
            // Plumbing settings to NIC failed
            MSMSEC_NIC_FAILURE = (MSMSEC_CONNECT_BASE + 16),
            // Operation was cancelled by caller
            MSMSEC_CANCELLED = (MSMSEC_CONNECT_BASE + 17),
            // Key was in incorrect format
            MSMSEC_KEY_FORMAT = (MSMSEC_CONNECT_BASE + 18),
            // Security downgrade detected
            MSMSEC_DOWNGRADE_DETECTED = (MSMSEC_CONNECT_BASE + 19),
            // PSK mismatch suspected
            MSMSEC_PSK_MISMATCH_SUSPECTED = (MSMSEC_CONNECT_BASE + 20),
            // Forced failure because connection method was not secure
            MSMSEC_FORCED_FAILURE = (MSMSEC_CONNECT_BASE + 21),
            // ui request couldn't be queued or user pressed cancel
            MSMSEC_SECURITY_UI_FAILURE = (MSMSEC_CONNECT_BASE + 22),

            MSMSEC_MAX = MSMSEC_END
        }


        /*
          DWORD WINAPI WlanOpenHandle(
         __in  DWORD dwClientVersion,
            PVOID pReserved,
         __out PDWORD pdwNegotiatedVersion,
         __out PHANDLE phClientHandle
          );
          - http://msdn2.microsoft.com/en-us/library/ms706759.aspx
        */
        [DllImport("wlanapi.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern UInt32 WlanOpenHandle(
         UInt32 dwClientVersion
         , IntPtr pReserved
         , out IntPtr pdwNegotiatedVersion
         , out IntPtr phClientHandle);


        /*
          DWORD WINAPI WlanCloseHandle(
         __in  HANDLE hClientHandle,
            PVOID pReserved
          );
          - http://msdn2.microsoft.com/en-us/library/ms706610.aspx
        */
        [DllImport("wlanapi.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern UInt32 WlanCloseHandle(
         IntPtr hClientHandle
         , IntPtr pReserved);



        /*
         typedef struct _WLAN_PROFILE_INFO_LIST { 
          DWORD dwNumberOfItems; 
          DWORD dwIndex; 
          WLAN_PROFILE_INFO ProfileInfo[1];
         } WLAN_PROFILE_INFO_LIST, 
         *PWLAN_PROFILE_INFO_LIST;   

         -http://msdn2.microsoft.com/en-us/library/ms707387.aspx
        */
        [StructLayout(LayoutKind.Sequential)]
        public struct WLAN_PROFILE_INFO_LIST
        {
            [MarshalAs(UnmanagedType.I4)]
            public Int32 dwNumberOfItems;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 dwIndex;
            [MarshalAs(UnmanagedType.ByValArray)]
            public WLAN_PROFILE_INFO[] ProfileInfo;
        }

        /*
          typedef struct _WLAN_INTERFACE_INFO {
         GUID InterfaceGuid;
         WCHAR strInterfaceDescription[256];
         WLAN_INTERFACE_STATE isState;
          } WLAN_INTERFACE_INFO,
          *PWLAN_INTERFACE_INFO;
          - http://msdn2.microsoft.com/en-us/library/ms706868.aspx
        */
        [StructLayout(LayoutKind.Sequential)]
        public struct WLAN_INTERFACE_INFO
        {
            public IntPtr InterfaceGuid;
            [MarshalAs(UnmanagedType.LPWStr, SizeConst = 256)]
            public String strInterfaceDescription; // string ma mat dlzku 256
            public uint isState;// toto si tiez nie sme isti
        }

        /*
          DWORD WINAPI WlanEnumInterfaces(
         __in          HANDLE hClientHandle,
         __in          PVOID pReserved,
         __out         PWLAN_INTERFACE_INFO_LIST* ppInterfaceList
          );
          - http://msdn2.microsoft.com/en-us/library/ms706716.aspx
        */
        [DllImport("wlanapi.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern UInt32 WlanEnumInterfaces(
         [In] IntPtr clientHandle,
         [In, Out] IntPtr pReserved,
         [Out] out IntPtr ppInterfaceList);



        /*
         DWORD WINAPI WlanGetProfile(
          __in        HANDLE hClientHandle,
          __in        const GUID* pInterfaceGuid,
          __in        LPCWSTR strProfileName,
          __reserved  PVOID pReserved,
          __out       LPWSTR* pstrProfileXml,
          __out_opt   DWORD* pdwFlags,
          __out_opt   PDWORD pdwGrantedAccess
         );
        */

        [DllImport("wlanapi.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern UInt32 WlanGetProfile(
         //   IntPtr hClientHandle,
         //   byte[] pInterfaceGuid,
         //   String strProfileName,
         //   IntPtr pReserved,
         //   out String pStrProfileXml,
         //   out IntPtr pdwFlags,
         //   out IntPtr pdwGrantedAccess
         //   );

         [In] IntPtr clientHandle,
         [In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
         [In, MarshalAs(UnmanagedType.LPWStr)] string profileName,
         [In] IntPtr pReserved,
         [Out] out string profileXml,
         [Out, Optional] out WlanProfileFlags flags,
         [Out, Optional] out WlanAccess grantedAccess
        );


        [DllImport("wlanapi.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern UInt32 WlanSetProfile(
         [In] IntPtr clientHandle,
         [In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
         [In] WlanProfileFlags flags,
         [In, MarshalAs(UnmanagedType.LPWStr)] string profileXml,
         [In, Optional, MarshalAs(UnmanagedType.LPWStr)] string allUserProfileSecurity,
         [In] bool overwrite,
         [In] IntPtr pReserved,
         [Out] out WlanReasonCode reasonCode
        );

        /*
         DWORD WINAPI WlanGetProfileList(
          HANDLE hClientHandle,
          const GUID* pInterfaceGuid,
          PVOID pReserved,
          PWLAN_PROFILE_INFO_LIST* ppProfileList
        );
         */

        [DllImport("wlanapi.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern UInt32 WlanGetProfileList(
         IntPtr hClientHandle
         , byte[] pInterfaceGuid
         , IntPtr pReserved
         , out WLAN_PROFILE_INFO_LIST ppProfileList
         );
    }


    public class WLAN_INTERFACE_INFO
    {
        public Guid InterfaceGuid;
        public string strInterfaceDescription;
        public WLAN_INTERFACE_STATE isState;

        [Flags]
        public enum WLAN_INTERFACE_STATE
        {
            wlan_interface_state_not_ready,
            wlan_interface_state_connected,
            wlan_interface_state_ad_hoc_network_formed,
            wlan_interface_state_disconnecting,
            wlan_interface_state_disconnected,
            wlan_interface_state_associating,
            wlan_interface_state_discovering,
            wlan_interface_state_authenticating
        }
    }


    public class WifiInterface
    {

        public static string GetProfileXML(Guid InterfaceGuid, String ProfileName)
        {
            uint r1, r2, r3;
            IntPtr reserved, clientHandle;
            WifiAPI.WlanProfileFlags pdwFlags;
            WifiAPI.WlanAccess pdwGrantedAccess;
            string profileXML;

            r1 = WifiAPI.WlanOpenHandle(1, IntPtr.Zero, out reserved, out clientHandle);
            r2 = WifiAPI.WlanGetProfile(clientHandle, InterfaceGuid, ProfileName, IntPtr.Zero, out profileXML, out pdwFlags, out pdwGrantedAccess);
            r3 = WifiAPI.WlanCloseHandle(clientHandle, IntPtr.Zero);

            //   StringBuilder sRet = new StringBuilder();
            //   sRet.Append("WlanOpenHandle return code: " + e1 + " reserved : " + reserved + "\r\n");
            //   sRet.Append("Client handle: " + clientHandle.ToString() + "\r\n");
            //   sRet.Append("WlanGetProfile return code: " + e2 + "\r\n");
            //   sRet.Append("WlanCloseHandle return code: " + e3 + "\r\n");

            return profileXML; // + "\r\n" + sRet.ToString();
        }
        /*
        public static bool SetProfile(Guid InterfaceGuid, String ProfileXML)
        {
            uint r1, r2, r3;
            IntPtr clientHandle, pReserved;
            WifiAPI.WlanReasonCode reasonCode;

            try
            {
                r1 = WifiAPI.WlanOpenHandle(1, IntPtr.Zero, out pReserved, out clientHandle);
                r2 = WifiAPI.WlanSetProfile(clientHandle, InterfaceGuid, 0, ProfileXML, null, true, IntPtr.Zero, out reasonCode);
                r3 = WifiAPI.WlanCloseHandle(clientHandle, IntPtr.Zero);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
        */

        public static WLAN_INTERFACE_INFO[] EnumInterfaces()
        {

            uint r1, r2, r3;
            IntPtr clientHandle, pReserved;
            IntPtr ppInterfaceList;
            //string[] arrGUID = null;

            WLAN_INTERFACE_INFO[] InterfaceInfo = null;
            try
            {
                r1 = WifiAPI.WlanOpenHandle(1, IntPtr.Zero, out pReserved, out clientHandle);
                r2 = WifiAPI.WlanEnumInterfaces(clientHandle, IntPtr.Zero, out ppInterfaceList);
                r3 = WifiAPI.WlanCloseHandle(clientHandle, IntPtr.Zero);


                int dwNumberOfItems = Marshal.ReadInt32(ppInterfaceList, 0);
                int dwIndex = Marshal.ReadInt32(ppInterfaceList, 4);
                InterfaceInfo = new WLAN_INTERFACE_INFO[dwNumberOfItems];

                for (int i = 0; i < dwNumberOfItems; i++)
                {
                    IntPtr pItemList = new IntPtr(ppInterfaceList.ToInt32() + (i * 284));
                    WLAN_INTERFACE_INFO wii = new WLAN_INTERFACE_INFO();

                    byte[] intGuid = new byte[16];
                    for (int j = 0; j < 16; j++)
                    {
                        intGuid[j] = Marshal.ReadByte(pItemList, 8 + j);
                    }
                    wii.InterfaceGuid = new Guid(intGuid);
                    //wii.InterfacePtr = new IntPtr(pItemList.ToInt32() + 8);
                    wii.strInterfaceDescription =
                     Marshal.PtrToStringUni(new IntPtr(pItemList.ToInt32() + 24), 256).Replace("\0", "");
                    wii.isState = (WLAN_INTERFACE_INFO.WLAN_INTERFACE_STATE)Marshal.ReadInt32(pItemList, 280);

                    InterfaceInfo[i] = wii;
                }

                return InterfaceInfo;
            }
            catch (Exception)
            {
                return InterfaceInfo;
            }


        }
    }
}
