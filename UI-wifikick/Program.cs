﻿#undef DEBUG

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;


namespace UI_wifikick
{
    
    static class Program
    {
        public static string OSName = string.Empty; // OS버전
        public static string Domain = string.Empty; // 도메인체크
        public static string Profile = string.Empty; // 프로필설정
        public static string DeviceId = string.Empty; // 프로필설정
        public static string IPAddress = string.Empty; // 프로필설정
        public static string GateWay = string.Empty; // 프로필설정
        public static string Desc = string.Empty; // 프로필설정
        public static bool Skip = false; // 프로필설정
        /// <summary>
        /// 해당 애플리케이션의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {

            /*
                 * 2020-06-26 AD이행 체크여부 변경
                 * 1) 프로세스 존재 확인 : ubgmc.exe
                   2) 레지스트리값 확인 : OR 조건으로 3개중 하나가 맞으면 OK 
                        HKEY_CURRENT_USER\Volatile Environment\USERDNSDOMAIN  (value : MOBIS.HKMG.GLOBAL)
                        HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Biosinfo\ADLogon (value : m0b!s@duser)
                        HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Domain (value : MOBIS.hkmg.global)
            
            try
            {

                
                Domain = System.Environment.UserDomainName;
                

                if (Domain.IndexOf("mobis", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    Console.Write(Domain);
                    Program.TXTLog("도메인: " + Domain);
                    // MOBIS로 가져옴
                }
                else
                {
                    Program.TXTLog("도메인: " + Domain);
#if DEBUG
                    //Environment.Exit(0);
#endif
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("PC정보를 읽어오는데 실패했습니다", "종료"
                                    , MessageBoxButtons.OK
                                    , MessageBoxIcon.Information);
                Environment.Exit(0); // AD가 이행이 되지 않은 PC는 예외
            }
            */

            if (checkProc() && (checkUserDomain() || checkADLogon() || checkDomain()))
            {
                Domain = "mobis";
#if DEBUG
                Program.TXTLog("도메인: " + Domain);
#endif
            }
            else
            {
                Domain = "Null";
#if DEBUG
                Program.TXTLog("도메인: " + Domain);
#endif
            }

            //  2. 윈도우7 인지 윈도우10 인지 체크
            OperatingSystem os = Environment.OSVersion;
            Version vs = os.Version;

            if (vs.Major == 6 && vs.Minor == 1)
            {
                OSName = "Windows 7";   // 윈도우 7
                                        //Console.Write("2.대상PC는 Windows 7\n");
                                        //Console.ReadLine();
#if DEBUG
                Program.TXTLog("OS네임: " + OSName);
#endif
            }
            else if (vs.Major == 6 && vs.Minor == 2)
            {
                OSName = "Windows 10";  // 윈도우 10
                                        //Console.Write("2.대상PC는 Windows 10\n");
                                        //Console.ReadLine();
#if DEBUG
                Program.TXTLog("OS네임: " + OSName);
#endif
            }
            else
            {
                OSName = "2.Unknown"; //대상 아님
#if DEBUG
                //Console.Write("2.대상PC OS정보: " + OSName + "\n");
#endif
                MessageBox.Show("대상OS가 올바르지않습니다", "종료"
                                    , MessageBoxButtons.OK
                                    , MessageBoxIcon.Information);
                Environment.Exit(0); 
            }

           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            
            /*
            Form2 showForm2 = new Form2();
            Application.Run(showForm2);
            showForm2.Show();
            */

        }
        public static void TXTLog(String strMsg)
        {
            try
            {
                string m_strLogPrefix = AppDomain.CurrentDomain.BaseDirectory + @"LOG\";
                string m_strLogExt = @".LOG";
                DateTime dtNow = DateTime.Now;
                string strDate = dtNow.ToString("yyyy-MM-dd");
                string strPath = String.Format("{0}{1}{2}", m_strLogPrefix, strDate, m_strLogExt);
                string strDir = Path.GetDirectoryName(strPath);
                DirectoryInfo diDir = new DirectoryInfo(strDir);

                if (!diDir.Exists)
                {
                    diDir.Create();
                    diDir = new DirectoryInfo(strDir);
                }

                if (diDir.Exists)
                {
                    System.IO.StreamWriter swStream = File.AppendText(strPath);
                    string strLog = String.Format("{0}: {1}", dtNow.ToString(dtNow.Hour + "시mm분ss초"), strMsg);
                    swStream.WriteLine(strLog);
                    swStream.Close(); ;
                }
            }
            catch (System.Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        public static Boolean checkUserDomain()
        {
            RegistryKey reg = Registry.CurrentUser;
            reg = reg.OpenSubKey("Volatile Environment", true);

            if (reg != null)
            {
                Object val = reg.GetValue("USERDNSDOMAIN");
                if (null != val)
                {
                    String chk = val.ToString();

                    if (chk.IndexOf("MOBIS.HKMG.GLOBAL", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
#if DEBUG
                        Program.TXTLog("USERDNSDOMAIN: " + val.ToString());
#endif
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }

            return false;
        }

        public static Boolean checkADLogon()
        {
            RegistryKey reg = Registry.LocalMachine;
            reg = reg.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Biosinfo", true);

            if (reg != null)
            {
                Object val = reg.GetValue("ADLogon");
                
                if (null != val)
                {
                    String chk = val.ToString();

                    if (chk.IndexOf("m0b!s@duser", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
#if DEBUG
                        Program.TXTLog("m0b!s@duser: " + val.ToString());
#endif
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }

        public static Boolean checkDomain()
        {
            RegistryKey reg = Registry.LocalMachine;
            reg = reg.OpenSubKey("SYSTEM\\CurrentControlSet\\services\\Tcpip\\Parameters", true);

            if (reg != null)
            {
                Object val = reg.GetValue("Domain");

                if (null != val)
                {
                    String chk = val.ToString();

                    if (chk.IndexOf("MOBIS.hkmg.global", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
#if DEBUG
                        Program.TXTLog("Domain: " + val.ToString());
#endif
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }
        public static Boolean checkProc()
        {
            Process[] processList = Process.GetProcessesByName("ubgmc");

            if (processList.Length > 1)
            {
                //Domain = "Null";
                //Program.TXTLog("ubgmc 체크 없음" + Domain);
                return false;
            }
            return true;
            
        }
    }
}
