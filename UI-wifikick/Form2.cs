﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.IO;
using System.Reflection;
using System.Drawing.Text;
using System.Text.RegularExpressions;

namespace UI_wifikick
{
    public partial class Form2 : MetroFramework.Forms.MetroForm

    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
            IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);

        private PrivateFontCollection fonts = new PrivateFontCollection();

        Font myFont;

        public Form2()
        {
            InitializeComponent();
            
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle; // 크기 고정
            this.MaximizeBox = false; // 최대화 버튼 무효
            this.FormBorderStyle = FormBorderStyle.None;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width / 2 - this.Size.Width / 2, Screen.PrimaryScreen.Bounds.Height / 2 - this.Size.Height / 2);

            byte[] fontData = Properties.Resources.BinggraeMelona;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.BinggraeMelona.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.BinggraeMelona.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);

            myFont = new Font(fonts.Families[0], 9f);

            /*
            //button1_Click(sender, e);
            PrivateFontCollection privateFonts = new PrivateFontCollection();
            privateFonts.AddFontFile("BinggraeMelona.ttf");
            Font font = new Font(privateFonts.Families[0], 9f);
            */

            label1.Font = myFont;
            label7.Font = myFont;
            label8.Font = myFont;
            label9.Font = myFont;
            button1.Font = myFont;
            button2.Font = myFont;
            button3.Font = myFont;
            button4.Font = myFont;

            Font font1 = new Font(fonts.Families[0], 15f);
            label10.Font = font1;

            
            textBox1.MaxLength = 3;
            textBox2.MaxLength = 3;
            textBox3.MaxLength = 3;
            textBox4.MaxLength = 3;
            this.Activate();
            textBox1.Focus();
            //this.ActiveControl = textBox1;
            this.textBox1.TextChanged += new EventHandler(FocusMove);
            this.textBox2.TextChanged += new EventHandler(FocusMove);
            this.textBox3.TextChanged += new EventHandler(FocusMove);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
             MessageBox.Show("무선설정완료 후 IP를 별도 입력하세요", "안내"
                                     , MessageBoxButtons.OK
                                     , MessageBoxIcon.Information);
            /*
             *MessageBox.Show("IP까지 한번에 설정하는 기능은 추후 개선예정입니다", "안내"
                                    , MessageBoxButtons.OK
                                    , MessageBoxIcon.Information);

            */

            Program.Skip = true;
            this.Visible = false;             // 추가
            Form3 showForm3 = new Form3();
            showForm3.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Visible = false;             // 추가
            Form1 showForm1 = new Form1();
            showForm1.ShowDialog();
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedTextBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedTextBox3_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedTextBox4_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(textBox1.Text);
            if ((textBox1.Text.ToString() == string.Empty)|| (textBox2.Text.ToString() == string.Empty) || (textBox3.Text.ToString() == string.Empty) || (textBox4.Text.ToString() == string.Empty))
            {
                MessageBox.Show("빈값은 입력할 수 없습니다.");
                return;
            }

            int a = Int32.Parse(textBox1.Text.ToString());
            int b = Int32.Parse(textBox2.Text.ToString());
            int c = Int32.Parse(textBox3.Text.ToString());
            int d = Int32.Parse(textBox4.Text.ToString());

            /*
            MessageBox.Show(a.ToString());
            MessageBox.Show(b.ToString());
            MessageBox.Show(c.ToString());
            MessageBox.Show(d.ToString());
            */

            if((a<0 || a>255) || (b < 0 || b > 255) || (c < 0 || c > 255) || (d < 0 || d > 255))
            {
                MessageBox.Show("0~255 사이의 값을 다시 입력하세요.");
                return;
                // 성공
            }

            Program.IPAddress = a.ToString() + "." + b.ToString() + "." + c.ToString() + "." + d.ToString();
            //MessageBox.Show(Program.IPAddress);
            Program.GateWay = a.ToString() + "." + b.ToString() + "." + c.ToString() + ".1";
            //MessageBox.Show(Program.GateWay);

            Program.Skip = false;
            this.Visible = false;             // 추가
            Form3 showForm3 = new Form3();
            showForm3.ShowDialog();
            this.Close();
        }

        
        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)'.')
            {
                textBox2.Focus();
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                button4_Click(sender, e);
            }
            else
            {
                return;
            }
        }
        

        private void textBox2_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 8 && textBox2.Text == "")
            {
                textBox1.Focus();
                return;
            }

            
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8 )
            {
                e.Handled = true;
            }

            if (e.KeyChar == (char)'.')
            {
                textBox3.Focus();
                return;
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                button4_Click(sender, e);
                return;
            }
            else
            {
                return;
            }
        }

        private void textBox3_KeyPress_1(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == 8 && textBox3.Text == "")
            {
                textBox2.Focus();
                return;
            }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8)
            {
                e.Handled = true;
            }

            if (e.KeyChar == (char)'.')
            {
                textBox4.Focus();
                return;
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                button4_Click(sender, e);
                return;
            }
            else
            {
                return;
            }
        }

        private void textBox4_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 8 && textBox4.Text == "")
            {
                textBox3.Focus();
                return;
            }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8)
            {
                e.Handled = true;
            }

            if (e.KeyChar == (char)Keys.Enter)
            {
                button4_Click(sender, e);
                return;
            }
            else
            {
                return;
            }
        }
        private void FocusMove(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txt.Text.Length == 3)
            {
                SendKeys.Send("{tab}");
            }
        }
    }
}
