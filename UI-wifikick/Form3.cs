﻿#undef DEBUG

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using NativeWifi;


namespace UI_wifikick
{
    public partial class Form3 : MetroFramework.Forms.MetroForm
    {

        string setXml = string.Empty;
        static string index = string.Empty;
        static string serviceID = string.Empty;
        static string tmp = string.Empty;
        static string chk = string.Empty;

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
            IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);

        private PrivateFontCollection fonts = new PrivateFontCollection();

        Font myFont;
        public Form3()
        {
            InitializeComponent();
            FormClosing += new FormClosingEventHandler(closing);

            switch (Program.Profile.ToString())
            {
                case "MOBIS WiFi":
                    setXml = Properties.Resources.mobiswifi;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ "+ Program.Profile.ToString() + " ]\n");
                    break;

                case "MOBIS WiFi_AD":
                    setXml = Properties.Resources.mobiswifi_AD;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + "mobiswifi" + " ]\n");
                    break;

                case "MOBIS WiFi BT":
                    setXml = Properties.Resources.mobiswifi_BT;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + Program.Profile.ToString() + " ]\n");
                    break;

                case "MOBIS WiFi BT_AD":
                    setXml = Properties.Resources.mobiswifi_BT_AD;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + "mobiswifi" + " ]\n");
                    break;

                case "MOBIS WiFi TP":
                    setXml = Properties.Resources.mobiswifi_TP;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + Program.Profile.ToString() + " ]\n");
                    break;

                case "MOBIS WiFi TP_AD":
                    setXml = Properties.Resources.mobiswifi_TP_AD;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + "mobiswifi_TP" + " ]\n");
                    break;

                case "MOBIS WiFi CH":
                    setXml = Properties.Resources.mobiswifi_CH;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + Program.Profile.ToString() + " ]\n");
                    break;

                case "MOBIS WiFi CH_AD":
                    setXml = Properties.Resources.mobiswifi_CH_AD;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + "mobiswifi_CH" + " ]\n");
                    break;

                case "MOBIS WiFi EE":
                    setXml = Properties.Resources.mobiswifi_EE;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + Program.Profile.ToString() + " ]\n");
                    break;

                case "MOBIS WiFi EE_AD":
                    setXml = Properties.Resources.mobiswifi_EE_AD;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + "mobiswifi_EE1" + " ]\n");
                    break;

                case "MOBIS WiFi SW":
                    setXml = Properties.Resources.mobiswifi_SW;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + Program.Profile.ToString() + " ]\n");
                    break;

                case "MOBIS WiFi SW_AD":
                    setXml = Properties.Resources.mobiswifi_SW_AD;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + "mobiswifi_SW" + " ]\n");
                    break;

                case "MOBIS WiFi EW":
                    setXml = Properties.Resources.mobiswifi_EW;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + Program.Profile.ToString() + " ]\n");
                    break;

                case "MOBIS WiFi EW_AD":
                    setXml = Properties.Resources.mobiswifi_EW_AD;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + "mobiswifi_EW" + " ]\n");
                    break;

                case "MOBIS WiFi SS":
                    setXml = Properties.Resources.mobiswifi_SS;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + Program.Profile.ToString() + " ]\n");
                    break;

                case "MOBIS WiFi SS_AD":
                    setXml = Properties.Resources.mobiswifi_SS_AD;
                    //MessageBox.Show(setXml);
                    richTextBox1.Text = "1. PC확인 --- [ 성 공 ]\n";
                    richTextBox1.AppendText("2. SSID확인 --- [ " + "mobiswifi_SS" + " ]\n");
                    break;

                default:
                    richTextBox1.Text = "1. PC확인 --- [ 실 패 ]\n";
                    Program.TXTLog(Program.Profile);
                    break;
            }


            // ThreadException이벤트·핸들러를 등록한다
            Application.ThreadException += new
              ThreadExceptionEventHandler(Application_ThreadException);

            // UnhandledException이벤트·핸들러를 등록한다
            Thread.GetDomain().UnhandledException += new
              UnhandledExceptionEventHandler(Application_UnhandledException);
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            this.Visible = false;             // 추가
            Form1 showForm1 = new Form1();
            showForm1.ShowDialog();
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 모비스 PC 확인 완료
            // 대상PC는 윈도우
            // 무선LAN 검색 성공
            // 무선LAN 활성화
            // 무선LAN 설정 세팅 성공
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle; // 크기 고정
            this.MaximizeBox = false; // 최대화 버튼 무효
            this.FormBorderStyle = FormBorderStyle.None;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width / 2 - this.Size.Width / 2, Screen.PrimaryScreen.Bounds.Height / 2 - this.Size.Height / 2);

            byte[] fontData = Properties.Resources.BinggraeMelona;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.BinggraeMelona.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.BinggraeMelona.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);

            myFont = new Font(fonts.Families[0], 9f);

            /*
            PrivateFontCollection privateFonts = new PrivateFontCollection();
            privateFonts.AddFontFile("BinggraeMelona.ttf");
            Font font = new Font(privateFonts.Families[0], 9f);
            */

            label1.Font = myFont;
            label7.Font = myFont;
            label8.Font = myFont;
            label9.Font = myFont;
            button1.Font = myFont;
            button2.Font = myFont;
            button3.Font = myFont;


            Font font1 = new Font(fonts.Families[0], 15f);
            label2.Font = font1;

            this.Activate();
            button1.Focus();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        private void closing(object sender, FormClosingEventArgs e)
        {
            // 폼을 닫을건지 취소할건지 결정
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                progressBar1.PerformStep();
            }

            FindReg();
            SetWifiID();
            for(int i=0; i<10; i++)
            {
                progressBar1.PerformStep();
            }
            
            SetWifiActive();
            for (int i = 0; i < 20; i++)
            {
                progressBar1.PerformStep();
            }

            if (Program.DeviceId == string.Empty)
            {
                MessageBox.Show("무선LAN Device를 찾을 수 없습니다. 프로그램을 종료합니다.", "안내"
                                    , MessageBoxButtons.OK
                                    , MessageBoxIcon.Information);
                Program.TXTLog("deviceID: " + Program.DeviceId);
                Delay(250);
                Environment.Exit(0); // 윈도우 7, 10 외 기타OS
            }
            
            
            richTextBox1.AppendText("3. 무선LAN Device 활성화 --- [ 성 공 ]\n");
            richTextBox1.AppendText("4. 무선LAN 설정대상 로딩 --- [ 성 공 ]\n");
            
            try
            {
                WlanClient client = new WlanClient();

                foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
                {
                    if (wlanIface.InterfaceDescription.Equals(Program.Desc))
                    {
                        try
                        {
                            // 1. 기존 profile 삭제하는 메소드인데 삭제하는걸 넣어야하는지??? 말아야하는지 사용자 측면 고려해봐야함
                            foreach (Wlan.WlanProfileInfo info in wlanIface.GetProfiles())
                            {
                                if (info.profileName != null)
                                {
                                    wlanIface.DeleteProfile(info.profileName);
                                    Program.TXTLog("삭제한 profile: " + info.profileName);
                                    Delay(250);
                                }
                            }
                            
                            wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, setXml, true);
                            Program.TXTLog("프로파일 적용 완료");

                        }
                        catch (System.ArgumentException ex)
                        {
                            Program.TXTLog(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Program.TXTLog(ex.Message);
                            //Console.WriteLine("Error Occured!");
                            //throw;
                        }
                        try
                        {
                            wlanIface.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, "MOBIS WiFi");
                            richTextBox1.AppendText("5. 무선LAN 설정 적용 --- [ 완 료 ]\n");
                            Program.TXTLog("무선랜에 XML설정 " + wlanIface.ToString());
                        }
                        catch (System.ArgumentException ex)
                        {
                            Program.TXTLog(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            richTextBox1.AppendText("무선XML 설정에 실패했습니다");
                            Program.TXTLog(ex.Message);
                            //Console.WriteLine("Error Occured!");
                            //throw;
                        }
                        for (int i = 0; i < 20; i++)
                        {
                            progressBar1.PerformStep();
                        }
                        int timeout = 3000;

                        try
                        {

                            while ((wlanIface.InterfaceState.ToString() != "Connected") && (timeout >= 0))
                            {
                                try
                                {
                                    Program.TXTLog("중간 인터페이스 상태: " + wlanIface.InterfaceState.ToString());
                                }
                                catch (System.ArgumentException ex)
                                {
                                    Program.TXTLog(ex.Message);
                                }
                                catch (Exception ex)
                                {
                                    Program.TXTLog("중간 인터페이스 상태찍는거에서 에러, wlanIface.InterfaceState.ToString()");
                                    Program.TXTLog(ex.Message);
                                }
                                try
                                {
                                    //System.Threading.Thread.Sleep(500);
                                    Program.TXTLog("Delay 전");
                                    Delay(250);
                                    Program.TXTLog("Delay 후");
                                }
                                catch (System.ArgumentException ex)
                                {
                                    Program.TXTLog(ex.Message);
                                }
                                catch (Exception ex)
                                {
                                    Program.TXTLog("중간 인터페이스 while문에서 exception");
                                    Program.TXTLog(ex.Message);
                                }
                                timeout = timeout - 500;
                            }
                        }
                        catch (System.ArgumentException ex)
                        {
                            Program.TXTLog(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Program.TXTLog(ex.Message);
                        }
                        for (int i = 0; i < 20; i++)
                        {
                            progressBar1.PerformStep();
                        }

                        //Program.TXTLog("최종 " + wlanIface.InterfaceState.ToString() + wlanIface);

                        if (wlanIface.InterfaceState.ToString() == "Connected")
                        {
                            Program.TXTLog("무선랜 인터페이스: Connected");
                        }
                        else
                        {
                            richTextBox1.AppendText("무선랜장치 활성화");
                        }
                    }
                }
            }
            catch (System.ArgumentException ex)
            {
                Program.TXTLog(ex.Message);
            }
            catch (Exception ex)
            {
                Program.TXTLog(ex.Message);
            }

            /*
            if(Program.Skip == false)
            {
                NetworkInterface[] networks = null;
                string service_name = string.Empty;
                try
                {
                    networks = NetworkInterface.GetAllNetworkInterfaces();
                }
                catch (Exception ex)
                {
                    Program.TXTLog(ex.Message);
                }
                try
                {
                    foreach (NetworkInterface net in networks)
                    {
                        if (serviceID.Equals(net.Id))
                        {
                            service_name = net.Name;
                            Program.TXTLog("설정할 네트워크 이름: " + service_name);
                        }
                        else
                        {
                            Program.TXTLog("비대상 네트워크 이름: " + net.Name);
                        }
                    }
                }
                catch(Exception ex)
                {
                    Program.TXTLog(ex.Message);
                }
                

                if (Program.OSName.Equals("Windows 7"))
                {
                    string argumentsIP = "interface ip set address \"" + service_name + "\" static " + Program.IPAddress + " 255.255.255.0 " + Program.GateWay;
                    //Program.TXTLog("윈7 IP설정 명령어 : " + argumentsIP);
                    try
                    {
                        ProcessStartInfo procStartInfoIP = new ProcessStartInfo("netsh", argumentsIP);
                        procStartInfoIP.RedirectStandardOutput = true;
                        procStartInfoIP.UseShellExecute = false;
                        procStartInfoIP.CreateNoWindow = true;

                        Process proc = Process.Start(procStartInfoIP);
                        proc.WaitForExit();
                        Program.TXTLog(proc.ExitCode.ToString());
                    }
                    catch (Exception ex)
                    {
                        Program.TXTLog(ex.Message);
                    }

                    string argumentsDNS = "interface ip set dnsservers \"" + service_name + "\" static 10.240.31.130 primary";
                    //Program.TXTLog("윈7 DNS설정 명령어 : " + argumentsDNS);

                    try
                    {
                        ProcessStartInfo procStartInfoDNS = new ProcessStartInfo("netsh", argumentsDNS);
                        procStartInfoDNS.RedirectStandardOutput = true;
                        procStartInfoDNS.UseShellExecute = false;
                        procStartInfoDNS.CreateNoWindow = true;

                        Process proc = Process.Start(procStartInfoDNS);
                        proc.WaitForExit();
                        Program.TXTLog(proc.ExitCode.ToString());
                    }
                    catch (Exception ex)
                    {
                        Program.TXTLog(ex.Message);
                    }
                } 
                else
                {
                   string argumentsIP = "-c int ip set address name=\"" + service_name + "\" source=static addr=" + Program.IPAddress + " mask=255.255.255.0 gateway=" + Program.GateWay + " gwmetric=0";
                   Program.TXTLog("윈10 IP설정 명령어 : " + argumentsIP);

                   try
                   {
                        ProcessStartInfo procStartInfoIP = new ProcessStartInfo("netsh", argumentsIP);
                        procStartInfoIP.RedirectStandardOutput = true;
                        procStartInfoIP.UseShellExecute = false;
                        procStartInfoIP.CreateNoWindow = true;

                        Process proc = Process.Start(procStartInfoIP);
                        proc.WaitForExit();
                        Program.TXTLog(proc.ExitCode.ToString());

                    }
                   catch(Exception ex)
                   {
                       Program.TXTLog(ex.Message);
                   }

                   string argumentsDNS = "-c int ip set dns name=\"" + service_name + "\" source=static addr=10.240.31.130 register=PRIMARY"+ " no";
                   Program.TXTLog("윈10 DNS설정 명령어 : " + argumentsDNS);
                    try
                    {
                        ProcessStartInfo procStartInfoDNS = new ProcessStartInfo("netsh", argumentsDNS);
                        procStartInfoDNS.RedirectStandardOutput = true;
                        procStartInfoDNS.UseShellExecute = false;
                        procStartInfoDNS.CreateNoWindow = true;

                        Process proc = Process.Start(procStartInfoDNS);
                        proc.WaitForExit();
                        Program.TXTLog(proc.ExitCode.ToString());
                    }
                    catch (Exception ex)
                    {
                        Program.TXTLog(ex.Message);
                    }

                }
                for (int i = 0; i < 20; i++)
                {
                    progressBar1.PerformStep();
                }
            }
            else
            {
                for (int i = 0; i < 20; i++)
                {
                    progressBar1.PerformStep();
                }
            }
            */

            MessageBox.Show(new Form { TopMost = true }, "프로그램을 종료합니다.", "안내", MessageBoxButtons.OK);
            /*
            MessageBox.Show("프로그램을 종료합니다.", "안내"
                                , MessageBoxButtons.OK
                                , MessageBoxIcon.Information);
                                */
            Program.TXTLog("프로그램 종료");
            Environment.Exit(0);
            Delay(250);
            
        }

        public static void FindReg()
        {
            //string[] sGuid = Helper.GetRegSubKey(regPath);
            //string[,] arrWifiInfo = new string[sGuid.Length, 5];
            string regPath = string.Empty;
            if (Program.OSName == "Windows 7")
                //regPath = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkCards";
                regPath = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkCards";
            else if (Program.OSName == "Windows 10")
                //regPath = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkCards";
                regPath = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkCards";

            //무선 네트워크의 SettingID 가져오기
            //string[] sGuid = Helper.GetRegSubKey(regPath);
            //string[,] arrWifiInfo = new string[sGuid.Length, 5];

            RegistryKey regKey = Registry.LocalMachine.OpenSubKey(regPath, RegistryKeyPermissionCheck.ReadWriteSubTree);
            string[] subKeynames = regKey.GetSubKeyNames();

            foreach (String s in subKeynames)
            {
                RegistryKey serialcomm = regKey.OpenSubKey(s);
                chk = (serialcomm.GetValue("Description").ToString()); // 설명 Realtek PCIe GbE Family Controller 이런 값이 들어있음

                if (chk.IndexOf("wireless", StringComparison.OrdinalIgnoreCase) >= 0 || chk.IndexOf("Wi-Fi", StringComparison.OrdinalIgnoreCase) >= 0 || chk.IndexOf("WiFi", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    tmp = (serialcomm.GetValue("ServiceName").ToString());
                    serviceID = tmp;
                    Program.Desc = chk;
#if DEBUG
                    Program.TXTLog("serviceID: "+ serviceID);
#endif
                    Console.Write(tmp);
                    Console.ReadLine();
                    return ;
                }
            }
            // 
            MessageBox.Show(new Form { TopMost = true }, "무선랜카드 정보를 찾을 수 없습니다.", "안내", MessageBoxButtons.OK);
            Environment.Exit(0);
        }

        public static void FindSSID()
        {

        }

        public static void SetWifiID()
        {
            
            System.Management.ObjectQuery oQuery = new System.Management.ObjectQuery("select * from Win32_NetworkAdapterConfiguration where SettingID ='" + tmp + "'");
            ManagementObjectSearcher oSearcher = new ManagementObjectSearcher(oQuery);
            ManagementObjectCollection oReturnCollection = oSearcher.Get();

            
            foreach (ManagementObject oReturn in oReturnCollection)
            {
                /*
                Console.Write(oReturn["Index"].ToString());
                Console.Write(oReturn["SettingID"].ToString());
                Console.Write(oReturn["IPEnabled"].ToString());
                Console.Write(oReturn["DHCPEnabled"].ToString());
                Console.ReadLine();
                */


                System.Management.ObjectQuery oQuery2 = new System.Management.ObjectQuery("select * from Win32_NetworkAdapter where index=" + oReturn["Index"].ToString());
                ManagementObjectSearcher oSearcher2 = new ManagementObjectSearcher(oQuery2);
                ManagementObjectCollection oReturnCollection2 = oSearcher2.Get();

                index = oReturn["Index"].ToString();
#if DEBUG
                Program.TXTLog(oReturn.ToString());
#endif

                if (oReturnCollection2.Count > 0)
                {

                    foreach (ManagementObject oReturn2 in oReturnCollection2)
                    {
                        if (oReturn2["PNPDeviceID"] != null)
                        {
                            Program.DeviceId = oReturn2["PNPDeviceID"].ToString();
                            //oReturn2.InvokeMethod("Enable", null, null); // 무선활성화
                            Console.Write("3.무선LAN을 활성화합니다.\n");

                            //oReturn2.InvokeMethod("Disable", null, null);
                            //System.Threading.Thread.Sleep(1000);
                            Delay(250);

                            oReturn2.InvokeMethod("Enable", null, null);
#if DEBUG
                            Program.TXTLog("Enable 된 디바이스ID: " + Program.DeviceId.ToString());
#endif

                            //System.Threading.Thread.Sleep(1000);
                            Delay(250);
                            //string description = oReturn2["Description"] as string;
                            //return deviceID;
                            //break;
                        }
                        else
                        {
                            Program.DeviceId = string.Empty;
                            //Console.Write("3.무선LAN을 찾을 수 없습니다...\n");
                            //Console.Write("종료하려면 아무키나 누르세요...\n");
                            //Environment.Exit(0);
                            //Console.ReadLine();
                            //return deviceID;
                        }
                    }
                }

            }
        }
        public static void SetWifiActive()
        {
            /*
            try
            {
                System.Management.ObjectQuery oQuery = new System.Management.ObjectQuery("select * from Win32_NetworkAdapter where NetEnabled = false and index=" + index.ToString());
                ManagementObjectSearcher oSearcher = new ManagementObjectSearcher(oQuery);
                ManagementObjectCollection oReturnCollection = oSearcher.Get();

                foreach (ManagementObject oReturn in oReturnCollection)
                {
                    ManagementBaseObject enable = oReturn.InvokeMethod("Enable", null, null);
                }
            }
            catch (Exception ex)
            {
                Program.TXTLog(ex.Message);
            }
            */
            Delay(250);
        }
        public static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();

            if (null != identity)
            {
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            return false;
        }
        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.ApplicationExitCall)
            {
                Program.TXTLog(e.CloseReason.ToString());
            }
        }
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        /*
         * 기존Delay
         */
        public static DateTime Delay(int MS)
        {
            try
            {
                DateTime ThisMoment = DateTime.Now;
                TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
                DateTime AfterWards = ThisMoment.Add(duration);

                try
                {
                    while (AfterWards >= ThisMoment)
                    {
                        try
                        {
                            System.Windows.Forms.Application.DoEvents();
                            ThisMoment = DateTime.Now;
                            //Program.TXTLog("Delay 내부");
                        }
                        catch (System.ArgumentException ex)
                        {
                            Program.TXTLog(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Program.TXTLog(ex.Message);
                        }
                    }
                }
                catch (System.ArgumentException ex)
                {
                    Program.TXTLog(ex.Message);
                }
                catch (Exception ex)
                {
                    Program.TXTLog(ex.Message);
                }

            }
            catch (System.ArgumentException ex)
            {
                Program.TXTLog(ex.Message);
            }
            catch (Exception ex)
            {
                //Program.TXTLog("Delay 함수에서 Exception");
                Program.TXTLog(ex.Message);
            }
            try
            {
                return DateTime.Now;
            }
            catch (System.ArgumentException ex)
            {
                //Program.TXTLog("DateTime.Now 리턴 시 에러나옴");
                Program.TXTLog(ex.Message);
                return DateTime.Now;
            }
            catch (Exception ex)
            {
                //Program.TXTLog("DateTime.Now 리턴 시 에러나옴");
                Program.TXTLog(ex.Message);
                return DateTime.Now;
            }
            finally
            {

            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }
        // 미처리 예외를 캐치 하는 이벤트·핸들러
        // (Windows어플리케이션용)
        public static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Program.TXTLog(e.Exception + "Application_ThreadException에 의한 예외 통지입니다.");
        }

        // 미처리 예외를 캐치 하는 이벤트·핸들러
        // (주로 콘솔·어플리케이션용)
        public static void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            if (ex != null)
            {
                Program.TXTLog(ex.Message + "처리되지 않은 예외가 발생했다능");
            }
        }
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //Process.GetCurrentProcess().Close();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }
        public static string getServiceId()
        {
            return serviceID.ToString();
        }
    }
}
